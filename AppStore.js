import { createMemoryHistory } from 'history'
import { applyMiddleware, createStore, compose ,combineReducers} from 'redux'
import { routerMiddleware, connectRouter } from 'connected-react-router'
import logger from 'redux-logger'

import productReducer from './Reducer/productReducer'
import userReducer from './Reducer/userReducer'

const reducer = (history) => combineReducers({
    product: productReducer,
    user: userReducer,
    router: connectRouter(history)
})

export const history = createMemoryHistory()

export const store = createStore(
    reducer(history),
    compose(
        applyMiddleware(
            routerMiddleware(history),
            logger
        )
    )
)
export default store
