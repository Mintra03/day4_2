import { createStore, combineReducers } from 'redux';
import UserReducer from './Reducer/userReducer';
import ProductReducer from './Reducer/productReducer'

const reducers = combineReducers({
    user: UserReducer,
    products: ProductReducer
})

const store = createStore(reducers);
export default store;
