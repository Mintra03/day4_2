const reducer = ((state = [] ,action) => {
    switch(action.type) {
        case 'ADD_TODOS' :
            return [...state,{topic: action.topic,
                        completed: false}]

        case 'TOGGLE_TODOS' :
            return state.map((each,index) => {
                if(index === action.targetIndex) {
                    return {
                        ...each,
                        completed: !each.completed
                    }
                }
                return each
            })

        case 'REMOVE_TODOS' :
            return state.filter((num,index) => {
                return index !== action.targetIndex
            })

        default: 
        return state
    }
})

let state = []
 
state = reducer(state, {
    type: 'ADD_TODOS',
    topic: 'เอาผ้าไปซัก'
})

state = reducer(state, {
    type: 'ADD_TODOS',
    topic: 'รดน้ำต้นไม้'
})

state = reducer(state, {
    type: 'ADD_TODOS',
    topic: 'ซื้อพิซซ่า'
})

state = reducer(state, {
    type: 'REMOVE_TODOS',
    targetIndex: 1
})

state = reducer(state, {
    type: 'TOGGLE_TODOS',
    targetIndex: 1
})

state = reducer(state, {
    type: 'TOGGLE_TODOS',
    targetIndex: 1
})

console.log(state)