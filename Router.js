import React, { Component } from 'react'
import { Route, Switch, Redirect  } from 'react-router-native'
import { ConnectedRouter } from 'connected-react-router'
import Login from './Containers/Login'
import Profile from './Containers/Profile'
import Editprofile from './Containers/Editprofile'
import ListPage from './Containers/ListPage'
import Product from './Containers/Product'
import Editproduct from './Containers/Editproduct'
import Addproduct from './Containers/Addproduct'
// import listProduct from './Components/listProduct'
// import itemProduct from './Components/itemProduct'
import { store, history } from './AppStore.js'
import { Provider } from 'react-redux'
// import App from './App.js'

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route excat path='/Login' component={Login} />
                        <Route excat path='/Profile' component={Profile} />
                        <Route excat path='/Editprofile' component={Editprofile} />
                        <Route excat path='/ListPage' component={ListPage} />
                        <Route excat path='/Product' component={Product} />
                        <Route excat path='/Editproduct' component={Editproduct} />
                        <Route excat path='/Addproduct' component={Addproduct} />
                        <Redirect to='/Login' />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}

export default Router
