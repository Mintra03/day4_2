import React, { Component } from 'react';
import { Platform, StyleSheet, Image, View, TextInput } from 'react-native';
import { Button } from '@ant-design/react-native';
import { connect } from 'react-redux'

class Login extends Component {

    state = {
        Username: '',
        Password: '',
        // modalVisible: false,
        // isLogin: false
    }

    onClickLogin = () => {
        const {addTodo,addProduct}=this.props
        this.props.history.push('/ListPage',addTodo(this.state.Username),addProduct())
    }

    changeText = (field, text) => {
        this.setState({ [field]: text })
    }

    render() {
        return (
            <View style={styles.container} >
                <View style={styles.content}>
                    <View style={[styles.box1, styles.center]}>
                        <Image source={require('./3.jpeg')} style={[styles.image, styles.logo, styles.center]} />
                    </View>


                    <View style={[styles.box2, styles.center]}>
                            {/* <Text> {this.state.Username} {this.state.Password} </Text> */}
                            <View style={[styles.textInput1, styles.center]}>
                            <View style={styles.textInputBox}>       
                            <TextInput
                                    style={styles.headertext}
                                    placeholder='Username'
                                    onChangeText={value => { this.setState({ Username: value }) }}
                                />
                            </View>

                            <View style={styles.textInputBox}>
                                <TextInput
                                    style={styles.headertext}
                                    placeholder='Password'
                                    onChangeText={value => { this.setState({ Password: value }) }}
                                />
                            </View>
                            </View>


                        <View style={[styles.button, styles.center]}>
                            <Button onPress={() => { this.onClickLogin() }}>Login</Button>
                        </View>

                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state)=>{
    return {
        user:state.user
    }
  }
const mapDispatchToProps =(dispatch)=>{
    return {
        addTodo:(Username)=>{
            dispatch({
                type:'USER_LOGIN',
                Username: Username,
                Password: '',
                name: '',
                surname: ''
            })
        },
  
        addProduct:()=>{
          dispatch({
              type:'ADD_PRODUCT',
              image:'',
              name:''
          })
        }
    }
  }
  

export default connect(mapStateToProps, mapDispatchToProps)(Login)


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },
    content: {
        backgroundColor: '#B3B6B7',
        flex: 1,
        flexDirection: 'column'
    },
    headertext: {
        height: 40,
        borderColor: '#F5B041',
        borderWidth: 3,
        fontSize: 20,
    },
    box1: {
        backgroundColor: '#B3B6B7',
        flex: 1,
        flexDirection: 'column'
    },

    box2: {
        backgroundColor: '#B3B6B7',
        flex: 1,
        flexDirection: 'column'
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        borderRadius: 150,
        width: 250,
        height: 250
    },

    image: {
        backgroundColor: '#EC7063',
        margin: 50,
    },

    textInput1: {
        backgroundColor: '#F5B041',
        flex: 1,
        padding: 12,
        margin: 10
    },

    button: {
        backgroundColor: '#B3B6B7',
        flex: 1,
        margin: 40,
    },
    margin: {
        margin: 40,
    },

    textInput: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingLeft: 100,
        paddingRight: 100,
        backgroundColor: 'white',
      },
    textInputBox: {
        margin: 10,
        flexDirection: 'column',
    },
    buttonBox: {
        color: 'pink',
      }    
    
});
