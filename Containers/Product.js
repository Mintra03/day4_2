import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { Button, Icon, Flex } from '@ant-design/react-native';


class Product extends Component {

    goToListPage = () => {
        this.props.history.push('./ListPage')
    }

    goToEditproduct = () => {
        this.props.history.push('./Editproduct')
    }

    // UNSAFE_componentWillMount() {
    //     console.log(this.props)

    // }

    render() {
        return (
            <View style={styles.container} >
                <View style={[styles.header, styles.center]}>
                    <TouchableOpacity onPress={this.goToListPage}>
                        <View style={[styles.boxIcon, styles.center]}>
                            <Text style={styles.textHeader}> <Icon name="left" size="md" color="#34495E" /> </Text>
                            {/* <Text style={styles.headertext}> {"<"} </Text> */}
                        </View>
                    </TouchableOpacity>

                    <View style={[styles.boxheader, styles.center]}>
                        <Text style={styles.headertext}> Product </Text>
                    </View>
                </View>

                <View style={[styles.content, styles.center]}>
                    <Text style={styles.headertext}> Image </Text>
                    {/* <Text> {this.props.location.state.Username} </Text> */}
                    <Text style={styles.headertext}> Name </Text>
                </View>

                <View style={[styles.footer, styles.center]}>
                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToEditproduct}> Edit Product </Button>
                    </Flex.Item>
                </View>

            </View>


        );
    }
}
export default Product

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    boxheader: {
        backgroundColor: '#CFEE85',
        margin: 3,
        flexDirection: 'row',
        flex: 1,
    },
    boxIcon: {
        backgroundColor: '#CFEE85',
        margin: 5,
        flex: 0,
    },
    header: {
        backgroundColor: 'white',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 3,
    },
    headertext: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30
    },
    content: {
        backgroundColor: '#A5F1CD',
        flex: 1,
        flexDirection: 'column'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    boxfooter: {
        backgroundColor: 'green',
        marginHorizontal: 3,
        flexDirection: 'row',
        flex: 1,
    },
    footer: {
        backgroundColor: '#A5F1CD',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 0.6,
    },

});
