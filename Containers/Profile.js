import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Platform, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Button, Icon, Flex } from '@ant-design/react-native';

class Profile extends Component {

    goToListPage = () => {
        this.props.history.push('./ListPage')
    }

    goToEditprofile = () => {
        this.props.history.push('./Editprofile')
    }

    UNSAFE_componentWillMount() {
        console.log(this.props)
    }

    render() {

        const {user} = this.props
        console.log(user);

        return (
            <View style={styles.container} >
                <View style={[styles.header, styles.center]}>
                    <TouchableOpacity onPress={this.goToListPage}>
                        <View style={[styles.boxIcon, styles.center]}>
                            <Text style={styles.textHeader}> <Icon name="left" size="md" color="#34495E" /> </Text>
                        </View>
                    </TouchableOpacity>

                    <View style={[styles.boxheader, styles.center]}>
                        <Text style={styles.headertext}> My Profile </Text>
                    </View>
                </View>

                <View style={[styles.content, styles.center]}>
                    <Text style={styles.headertext}> Username </Text>
                    <Text style={styles.text}> :{account[account.length-1].Username} </Text>
                    <Text style={styles.headertext}> First name </Text>
                    <Text style={styles.text}>:{account[account.length-1].Firstname}</Text>
                    <Text style={styles.headertext}> Last name </Text>
                    <Text style={styles.text}>:{account[account.length-1].Lastname}</Text>
                </View>

                <View style={[styles.footer, styles.center]}>
                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToEditprofile}> Edit Profile </Button>
                    </Flex.Item>
                </View>

            </View>


        );
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Profile)

const mapStateToProps = (state)=>{
    return {
        user:state.user,
        products:state.products
    }
  }
const mapDispatchToProps =(dispatch)=>{
    return {
        addTodo:(Username)=>{
            dispatch({
                type:'USER_LOGIN',
                Username: Username,
                Password: '',
                name: '',
                surname: ''
            })
        },
  
        addProduct:()=>{
          dispatch({
              type:'ADD_PRODUCT',
              image:'',
              name:''
          })
        }
    }
  }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    boxheader: {
        backgroundColor: '#CFEE85',
        margin: 3,
        flexDirection: 'row',
        flex: 1,
    },
    boxIcon: {
        backgroundColor: '#CFEE85',
        margin: 5,
        flex: 0,
    },
    header: {
        backgroundColor: 'white',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 3,
    },
    headertext: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30
    },
    text: {
        fontSize: 20,
        color: 'white',
        left: 30,
    },
    content: {
        backgroundColor: '#A5F1CD',
        flex: 1,
        flexDirection: 'column'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    boxfooter: {
        backgroundColor: 'green',
        marginHorizontal: 3,
        flexDirection: 'row',
        flex: 1,
    },
    footer: {
        backgroundColor: '#A5F1CD',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 0.6,
    },

});
