import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Button, Icon, Flex } from '@ant-design/react-native';
import { connect } from 'react-redux'

class Addproduct extends Component {

    state = {
        image: '',
        name: ''
      }

    goToLogin = () => {
        this.props.history.push('/Login')
    }

    onClickSave = () => {
        const {addProduct}=this.props
        this.props.history.push('/ListPage',addProduct(this.state.image,this.state.name))
    }

    goToListPage = () => {
        this.props.history.push('/ListPage')
    }

    render() {
        return (
            <View style={[styles.container]}>
                <View style={[styles.header, styles.center]}>
                    <TouchableOpacity onPress={this.goToListPage}>
                        <View style={[styles.boxIcon, styles.center]}>
                            <Text style={styles.textHeader}> <Icon name="left" size="md" color="#34495E" /> </Text>
                        </View>
                    </TouchableOpacity>

                    <View style={[styles.box, styles.center]}>
                        <Text style={styles.textHeader}> Add Product </Text>
                    </View>
                </View>

                <View style={[styles.content]}>
                    <Text style={styles.textHead}> Img </Text>
                    {/* <Text> {this.state.Image} {this.state.Name} </Text> */}
                    <View style={[styles.textInput1, styles.center]}>
                        <TextInput
                            style={styles.textInput}
                            placeholder='URL'
                            onChangeText={value => { this.setState({ image: value }) }}
                        />
                    </View>

                    <Text style={styles.textHead}> Name </Text>
                    <View style={[styles.textInput1, styles.center]}>
                        <TextInput
                            style={styles.textInput}
                            placeholder='Name'
                            onChangeText={value => { this.setState({ name: value }) }}
                        />
                    </View>
                </View>

                <View style={[styles.footer]}>
                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToListPage}>Save</Button>
                    </Flex.Item>
                </View>

            </View>


        )
    }
}
export default Addproduct

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F5FCFF',
        flex: 1
    },
    textHeader: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    },
    textHead: {
        fontSize: 30,
        color: 'white',
        margin: 10,
    },
    text: {
        fontSize: 20,
        color: 'black',
        left: 30,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: '#474644',
        flexDirection: 'row',
    },
    content: {
        backgroundColor: '#B3B6B7',
        flex: 1,
        flexDirection: 'column'

    },
    footer: {
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    box: {
        backgroundColor: '#F4CAF9',
        flex: 1,

        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        backgroundColor: '#F4CAF9',
        flex: 0,

    },
    textInput: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingLeft: 100,
        paddingRight: 100,
        backgroundColor: 'white',
        margin:5,
    },

});
