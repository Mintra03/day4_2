import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Button, Icon, Flex } from '@ant-design/react-native';

class Editprofile extends Component {

    constructor(props) {
        super(props);
        const { account } = this.props
        this.state = {
            Username: account[account.length - 1].Username,
            Firstname: account[account.length - 1].Firstname,
            Lastname: account[account.length - 1].Lastname
        };
    }

    goToProfile = () => {
        const { addTodo } = this.props
        this.props.history.push('/Profile', addTodo(this.state.Username, this.state.Firstname, this.state.Lastname))
    }

    goToListPage = () => {
        this.props.history.push('/ListPage')
    }

    render() {
        const { account } = this.props
        return (
            <View style={[styles.container]}>
                <View style={[styles.header, styles.center]}>
                    <TouchableOpacity onPress={this.goToListPage}>
                        <View style={[styles.boxIcon, styles.center]}>
                            <Text style={styles.textHeader}> <Icon name="left" size="md" color="#34495E" /> </Text>
                        </View>
                    </TouchableOpacity>

                    <View style={[styles.box, styles.center]}>
                        <Text style={styles.textHeader}> Edit Profile </Text>
                    </View>
                </View>

                <View style={[styles.content]}>
                <Text style={styles.textHead}> Username </Text>
                    {/* <Text> {this.state.FirstName} {this.state.LastName} </Text> */}
                    <View style={[styles.textInput1, styles.center]}>
                        <TextInput
                            style={styles.textInput}
                            placeholder={account[account.length - 1].Username}
                            onChangeText={value => { this.setState({ Username: value }) }}
                            value={this.state.Username}
                        />
                    </View>

                    <Text style={styles.textHead}> First Name </Text>
                    <View style={[styles.textInput1, styles.center]}>
                        <TextInput
                            style={styles.textInput}
                            placeholder={account[account.length - 1].Firstname}
                            onChangeText={value => { this.setState({ Firstname: value }) }}
                            value={this.state.Firstname}
                        />
                    </View>

                    <Text style={styles.textHead}> Last Name </Text>
                    <View style={[styles.textInput1, styles.center]}>
                        <TextInput
                            style={styles.textInput}
                            placeholder={account[account.length - 1].Lastname}
                            onChangeText={value => { this.setState({ Lastname: value }) }}
                            value={this.state.Lastname}
                        />
                    </View>
                </View>

                <View style={[styles.footer]}>
                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToProfile}>Save</Button>
                    </Flex.Item>
                </View>

            </View>


        )
    }
}
export default Editprofile

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F5FCFF',
        flex: 1
    },
    textHeader: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    },
    textHead: {
        fontSize: 30,
        color: 'white',
        margin: 10,
    },
    text: {
        fontSize: 20,
        color: 'black',
        left: 30,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: '#474644',
        flexDirection: 'row',
    },
    content: {
        backgroundColor: '#B3B6B7',
        flex: 1,
        flexDirection: 'column'
    },
    footer: {
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    box: {
        backgroundColor: '#F4CAF9',
        flex: 1,

        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        backgroundColor: '#F4CAF9',
        flex: 0,

    },
    textInput: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingLeft: 100,
        paddingRight: 100,
        backgroundColor: 'white',
        margin: 5,
    },

});
