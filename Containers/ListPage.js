import React, { Component } from 'react';
import {  StyleSheet, Text, View, Modal, TouchableOpacity, Alert, Image, ScrollView } from 'react-native';
import { Button, Icon, Flex } from '@ant-design/react-native';
import { connect } from 'react-redux'
// import product from '../Reducer/productReducer';
// import itemProduct from '../Components/itemProduct'

class ListPage extends Component {

    state = {
        items: [],
        image: [],
        name: []
    }

    onShowModal() {
        this.setState({ modalVisible: true });
    }

    onCloseModal() {
        this.setState({ modalVisible: false, message: '' });
    }

    goToLogin = () => {
        this.props.history.push('/Login')
    }

    goToListPage = () => {
        this.props.history.push('/ListPage')
    }

    goToAddproduct = () => {
        this.props.history.push('/Addproduct')
    }

    goToProfile = () => {
        this.props.history.push('/Profile')
    }
    goToProduct = () => {
        this.props.history.push('/Product')
    }

    loadData() {
        const { products } = this.props
        let items = []
        for (let i = 0; i < products.length; i++) {
            items = [this.setState({ name: products[i].name })]

        }
        return items
    }


    render() {
        const {user} = this.props
        console.log(user);
        
        // const { modalVisible, message } = this.state;
        return (
            <View style={styles.container} >

                <View style={styles.header}>
                    <TouchableOpacity onPress={this.goToList}>
                        <View style={[styles.boxIcon, styles.center]}>
                            <Text style={styles.textHeader}> <Icon name="left" size="md" color="black" /> </Text><Text style={styles.headertext}> My List </Text>
                        </View>
                    </TouchableOpacity>


                    <View style={[styles.box, styles.center]}>
                        <Text style={styles.textHeader}> My List </Text>
                    </View>
                </View>


                <Modal
                    transparent={true}
                    visible={this.state.modalVisible}
                >
                    <TouchableOpacity
                        style={styles.modal}
                        onPress={() => { this.onCloseModal() }}>
                        <Image source={require('./10.jpg')} style={[styles.logo, styles.center]} />
                    </TouchableOpacity>
                </Modal>

                <ScrollView>
                    <View style={styles.content}>
                        <View style={styles.row}>
                            <View style={styles.box1}>
                                <TouchableOpacity
                                    style={styles.modal}
                                    onPress={() => { this.onShowModal('./10.jpg') }}>
                                    <Image source={require('./10.jpg')} style={[styles.logo, styles.center]} />
                                </TouchableOpacity>
                            </View>

                            <View style={styles.box2}>
                                <Image source={require('./11.jpg')} style={[styles.logo, styles.center]} />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.box1}>
                                <Image source={require('./12.jpg')} style={[styles.logo, styles.center]} />
                            </View>

                            <View style={styles.box2}>
                                <Image source={require('./13.jpg')} style={[styles.logo, styles.center]} />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.box1}>
                                <Image source={require('./14.jpg')} style={[styles.logo, styles.center]} />
                            </View>

                            <View style={styles.box2}>
                                <Image source={require('./15.jpg')} style={[styles.logo, styles.center]} />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.box1}>
                                <Image source={require('./17.jpg')} style={[styles.logo, styles.center]} />
                            </View>

                            <View style={styles.box2}>
                                <Image source={require('./18.jpg')} style={[styles.logo, styles.center]} />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.box1}>
                                <Image source={require('./19.jpg')} style={[styles.logo, styles.center]} />
                            </View>

                            <View style={styles.box2}>
                                <Image source={require('./20.jpg')} style={[styles.logo, styles.center]} />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.box1}>
                                <Image source={require('./21.jpg')} style={[styles.logo, styles.center]} />
                            </View>

                            <View style={styles.box2}>
                                <Image source={require('./22.jpg')} style={[styles.logo, styles.center]} />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.box1}>
                                <Image source={require('./23.jpg')} style={[styles.logo, styles.center]} />
                            </View>

                            <View style={styles.box2}>
                                <Image source={require('./25.jpg')} style={[styles.logo, styles.center]} />
                            </View>
                        </View>
                    </View>
                </ScrollView>

                <View style={styles.footer}>
                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToLogin}> Login </Button>
                    </Flex.Item>

                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToAddproduct}> Add </Button>
                    </Flex.Item>

                    <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                        <Button onPress={this.goToProfile}> Profile </Button>
                    </Flex.Item>
                </View>
            </View>

        );
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(ListPage)

const mapStateToProps = (state)=>{
    return {
        user:state.user
    }
  }
const mapDispatchToProps =(dispatch)=>{
    return {
        addTodo:(Username)=>{
            dispatch({
                type:'USER_LOGIN',
                Username: Username,
                Password: '',
                name: '',
                surname: ''
            })
        },
  
        addProduct:()=>{
          dispatch({
              type:'ADD_PRODUCT',
              image:'',
              name:''
          })
        }
    }
  }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    header: {
        backgroundColor: '#474644',
        alignItems: 'center',
    },
    headertext: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30
    },
    content: {
        backgroundColor: 'cyan',
        flex: 1,
        flexDirection: 'column'
    },
    box: {
        backgroundColor: '#FADBD8',
        flex: 1,

        alignItems: 'center',
        justifyContent: 'center'
    },

    box1: {
        backgroundColor: '#F4CAF9',
        flex: 1,
        margin: 14,
    },
    box2: {
        backgroundColor: '#F4CAF9',
        flex: 1,
        margin: 14,
    },
    row: {
        backgroundColor: '#F4CAF9',
        flex: 1,
        flexDirection: 'row'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        borderRadius: 10,
        width: 170,
        height: 170,
    },
    modal: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.8)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxfooter: {
        backgroundColor: 'green',
        marginHorizontal: 3,
        flexDirection: 'row',
        flex: 1,
    },
    boxIcon: {
        backgroundColor: '#474644',
        margin: 5,
        flex: 0,
    },

    footer: {
        backgroundColor: '#474644',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 8,
    },


});
