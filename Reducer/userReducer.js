export default (state = [], action) => {
    switch (action.type) {
        case 'USER_LOGIN':
            console.log('login action:', action)
            return [...state,{
                Username: action.Username,
                Password: '',
                name: '',
                surname: ''
            }]
        case 'USER_EDIT':
            return {
                ...state,
                name: action.name,
                surname: action.surname
            }
        case 'USER_LOGOUT':
            return {}
        default:
            return state
    }
}
