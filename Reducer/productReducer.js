export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return [...state,
            {
                Image: action.Image,
                Name: action.Name
            }
            ]
        case 'EDIT_PRODUCT':
            return state.filter((item, index) => { index !== action.index })
        default:
            return state

    }
}
